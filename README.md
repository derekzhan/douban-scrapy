#douban-scrapy

爬取豆瓣top250数据并保存到json文件，格式如下

> ```json```
{"quote": ["弱者送给弱者的一刀。"], "rate": ["8.7", "90237人评价"], "link": ["https://movie.douban.com/subject/1292329/"], "rank": ["200"], "title": ["牯岭街少年杀人事件"]}
{"quote": ["没有了退路，只好飞向自由。"], "rate": ["8.7", "91286人评价"], "link": ["https://movie.douban.com/subject/1291992/"], "rank": ["201"], "title": ["末路狂花"]}
